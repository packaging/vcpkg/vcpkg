./: vcpkg/ manifest

vcpkg/
{
  ./: toolsrc/

  toolsrc/
  {
    ./: vcpkg/exe{vcpkg} file{VERSION.txt}

    # Note that we need the explicit fsdir{} because our distribution won't
    # have this (empty) directory.
    #
    vcpkg/exe{vcpkg}: cxx{src/vcpkg/**     +src/vcpkg}   \
                      hxx{include/vcpkg/** +include/pch} \
                      fsdir{vcpkg/}

    cxx.poptions =+ "-I$src_base/include" -DDISABLE_METRICS=1 \
-DVCPKG_VERSION="+$version.revision-build2"

    # Currently upstream is full of warnings, so disable for now.
    #
    if ($cxx.class == 'gcc')
      cxx.coptions += -Wno-all -Wno-extra

    # For some standard libraries std::filesystem is in a separate library.
    #
    if ($cxx.stdlib == 'libstdc++')
      cxx.libs += -lstdc++fs
    elif ($cxx.stdlib == 'libc++')
      cxx.libs += -lc++experimental

    if ($cxx.target.class == 'windows')
      cxx.libs += bcrypt.lib
  }

  # The goal here is to distribute pretty much the entire vcpkg repository so
  # that the resulting package can be used in the same way as a repository
  # clone (which is the "standard" way of using vcpkg).
  #
  ./: docs/file{**}                 \
      ports/file{**}                \
      scripts/file{** -bootstrap.*} \
      triplets/file{**}             \
      file{.vcpkg-root}             \
      doc{LICENSE.txt CHANGELOG.md README.md}
}

# Standard install doesn't make much sense with the way vcpkg works so we are
# going to repurpose it to create a "vcpkg build root" (which should contain
# all the ports, script, etc). In other words, you should be able to do:
#
# b install config.install.root=/tmp/vcpkg
# vcpkg install --vcpkg-root /tmp/vcpkg libfoo
#
manifest@./: install = root/

vcpkg/
{
  # Only install what we explicitly list.
  #
  file{*}: install = false

  file{.vcpkg-root}@./: install = root/

  ports/file{*}: install         = root/ports/
  ports/file{*}: install.subdirs = true

  scripts/file{*}: install         = root/scripts/
  scripts/file{*}: install.subdirs = true

  triplets/file{*}: install         = root/triplets/
  triplets/file{*}: install.subdirs = true
}
